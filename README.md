# My dwm-colortags build 


![1](https://gitlab.com/roidm/dwm-colortags/-/raw/main/pics/20220126113615.png "Image 1")

# Some of the Patches

- barpadding 
- Hide / Restore Windows
- LG3D patch
- Scratchpads
- cfacts 
- dragmfact 
- movestack 
- vanity gaps
- colorful tags
- statuspadding 
- status2d
- (...more patches)


# Credits
- [chadwm](https://github.com/siduck/chadwm) 
- [bakkeby](https://github.com/bakkeby/dwm-flexipatch)
- [UtkarshVerma](https://github.com/UtkarshVerma/dwm-flexipatch)
